package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

public class Rectangle  implements FiguraGeometrica {
    private final double base;
    private final double alcada;
    
    public Rectangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduïu la longitud de la base del rectangle: ");
        this.base = scanner.nextDouble();
        System.out.print("Introduïu l'alcada del rectangle: ");
        this.alcada = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return base * alcada;
    }
    
    @Override
    public double calcularPerimetre() {
        return 2 * (base + alcada);
    }
}