package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

// Classe que representa un pentàgon
public class Pentagon implements FiguraGeometrica {
    private final double costat;
    
    public Pentagon() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduïu la longitud del costat del pentàgon: ");
        this.costat = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return 1.720477401 * costat * costat; // Fórmula de l'àrea d'un pentàgon regular
    }
    
    @Override
    public double calcularPerimetre() {
        return 5 * costat;
    }
}
